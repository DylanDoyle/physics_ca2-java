public class Vector3
{
    double x, y, z;

    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    double dotProduct(Vector3 vec1, Vector3 vec2)
    {
        return 0;
    }

    double dotProduct(Vector3 vec1)
    {
        return (this.x * vec1.x) + (this.y * vec1.y) + (this.z * vec1.z);
    }
    double dotProduct()
    {
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
    }

    Vector3 addVectors(Vector3 vec1, Vector3 vec2)
    {
        double fX = vec1.x + vec2.x;
        double fY = vec1.y + vec2.y;
        double fZ = vec1.z + vec2.z;
        return new Vector3(fX, fY, fZ);
    }

    Vector3 addVectors(Vector3 vec1)
    {
        double fX = vec1.x + this.x;
        double fY = vec1.y + this.y;
        double fZ = vec1.z + this.z;
        return new Vector3(fX, fY, fZ);
    }

    Vector3 setNetForce(Vector3 vec1, Vector3 vec2, Vector3 vec3)
    {
        double fX = vec1.x + vec2.x + vec3.x;
        double fY = vec1.y + vec2.y + vec3.y;
        double fZ = vec1.z + vec2.z + vec3.z;
        return new Vector3(fX, fY, fZ);
    }

    Vector3 subtractVectors(Vector3 vec2) {
        double fX = this.x - vec2.x;
        double fY = this.y - vec2.y;
        double fZ = this.z - vec2.z;
        return new Vector3(fX, fY, fZ);
    }

    Vector3 scalarMultiply(double x)
    {
        return new Vector3((this.getX() * x),(this.getY() * x), (this.getZ() * x));
    }

    public double findLength(Vector3 vec)
    {
        return Math.sqrt(vec.dotProduct(vec,vec));
    }

    public double findLength()
    {
        return Math.sqrt(this.dotProduct());
    }

    public Vector3 changeSigns()
    {
       return this.scalarMultiply(-1);
    }

    public Vector3 normalise()
    {
        if(findLength() != 0)
        {
            return this.scalarMultiply(1/findLength());
        }
        return new Vector3(0,0,0);
    }

    public Vector3 crossProduct(Vector3 vec1)
    {
        Vector3 crossVec = new Vector3(0,0,0);
        crossVec.x = (this.getY() * vec1.getZ()) - (this.getZ() * vec1.getY());
        crossVec.y = -((this.getX() * vec1.getZ()) - (this.getZ() * vec1.getX()));
        crossVec.z = (this.getX() * vec1.getY()) - (this.getY() * vec1.getX());

        return crossVec;
    }

    @Override
    public String toString() {
        return"(" + x +
                ", " + y +
                ", " + z + ")";
    }
}
