public class Ball
{
    double radius, mass, area, volume, fluidDensity, dragCoefficient ,userGrav;
    Vector3 initPos, gravityForce, acceleration, velocity, windForce, rotationForce, NetForces;
    public Ball(double radius, double density, Vector3 initPos, Vector3 velocity)
    {
        this.radius = radius;
        this.mass = calculateMass(radius, density);
        this.initPos = initPos;
        area = findArea(radius);
        this.velocity = velocity;
    }

    public Ball(double radius, double ballDensity, double fluidDensity, double dragCoefficient, Vector3 initPos,
                Vector3 initVelocity, Vector3 rotationForce, Vector3 windForce) {
        this.radius = radius;
        this.mass = calculateMass(radius, ballDensity);
        this.initPos = initPos;
        this.velocity = initVelocity;
        this.windForce = windForce;
        this.rotationForce = rotationForce;

        Vector3 vAVector = initVelocity.subtractVectors(windForce);
        double vALength = vAVector.findLength();
        Vector3 gravityForce = this.findCommonGravity();
        Vector3 dragForce = this.findDrag(fluidDensity, dragCoefficient, vALength, vAVector);
        Vector3 magnusForce = this.findMagnusForce(fluidDensity, rotationForce, vAVector, vALength);
        NetForces = new Vector3().setNetForce(magnusForce, gravityForce, dragForce);
        this.acceleration = this.findAccelerationFromForces(NetForces);
    }

    public Ball(double radius, double ballDensity, double fluidDensity, double dragCoefficient,
                Vector3 initPos, Vector3 initVelocity, Vector3 rotationForce, Vector3 windForce, double userGrav) {
        this.radius = radius;
        this.mass = calculateMass(radius, ballDensity);
        this.initPos = initPos;
        this.velocity = initVelocity;
        this.windForce = windForce;
        this.rotationForce = rotationForce;

        Vector3 vAVector = initVelocity.subtractVectors(windForce);
        double vALength = vAVector.findLength();
        Vector3 gravityForce = this.findCustomGravity(userGrav);
        Vector3 dragForce = this.findDrag(fluidDensity, dragCoefficient, vALength, vAVector);
        Vector3 magnusForce = this.findMagnusForce(fluidDensity, rotationForce, vAVector, vALength);
        NetForces = new Vector3().setNetForce(magnusForce, gravityForce, dragForce);
        this.acceleration = this.findAccelerationFromForces(NetForces);
    }

    private Vector3 findCustomGravity(double userGrav) {
        double mg = -mass * (userGrav);
        gravityForce = new Vector3(0,0,1).scalarMultiply(mg);
        return gravityForce;
    }

    private double calculateMass(double radius, double density) {
        return findVolume(radius) * density;
    }

    private double findVolume(double radius) {
        this.volume = 4f/3f * Math.PI * (radius * radius * radius);
        return this.volume;
    }

    private double findArea(double radius)
    {
        this.area = 4f * Math.PI * (radius * radius);
        return this.area;
    }

    Vector3 findCommonGravity()
    {
        double mg = -mass * (9.81f);
        gravityForce = new Vector3(0,0,1).scalarMultiply(mg);
        return gravityForce;
    }

    public Vector3 findDrag(double fluidDensity, double dragCoefficient, double vALength, Vector3 vAVector) {
        Vector3 vAHat = new Vector3(0,0,0);
        if(vALength != 0)
        {
            vAHat = vAVector.scalarMultiply(1f/vALength);
        }
        double dragLength = (1f/2f * fluidDensity) * (Math.PI * (radius * radius)) * dragCoefficient * (vALength * vALength);

        return vAHat.scalarMultiply(-dragLength);
    }

    public Vector3 findMagnusForce(double fluidDensity, Vector3 rotation, Vector3 vaVector, double vaLength)
    {
        double Cl = 0;
        if(vaLength != 0)
        {
             Cl = (radius * rotation.findLength()) / vaLength;
        }

        double magnusLength = (1f/2f * fluidDensity) * (Math.PI * (radius * radius)) * Cl * (vaLength * vaLength);
        Vector3 crossResult = rotation.crossProduct(vaVector);
        Vector3 normalisedCross = crossResult.normalise();
        return normalisedCross.scalarMultiply(magnusLength);
    }

    public Vector3 findAccelerationFromForces(Vector3 netForces)
    {
        this.acceleration = netForces.scalarMultiply(1/mass);
        return this.acceleration;
    }

    public Vector3 findAccelerationFromVelocity(Vector3 velocity)
    {
        Vector3 vAVector = velocity.subtractVectors(windForce);
        double vALength = vAVector.findLength();
        Vector3 gravityForce = this.findCustomGravity(userGrav);
        Vector3 dragForce = this.findDrag(fluidDensity, dragCoefficient, vALength, vAVector);
        Vector3 magnusForce = this.findMagnusForce(fluidDensity, rotationForce, vAVector, vALength);
        Vector3 netForce = new Vector3().setNetForce(magnusForce, gravityForce, dragForce);
        this.acceleration = this.findAccelerationFromForces(netForce);
        return this.acceleration;
    }

    @Override
    public String toString()
    {
        return "Ball{" +
                "radius=" + radius +
                ", mass=" + mass +
                ", area=" + area +
                ", volume=" + volume +
                ", \nfluidDensity=" + fluidDensity +
                ", dragCoefficient=" + dragCoefficient +
                ", userGrav=" + userGrav +
                ", initPos=" + initPos +
                ", \ngravityForce=" + gravityForce +
                ", acceleration=" + acceleration +
                ", \nvelocity=" + velocity +
                ", windForce=" + windForce +
                ", \nrotationForce=" + rotationForce +
                ", NetForces=" + NetForces +
                '}';
    }
}
