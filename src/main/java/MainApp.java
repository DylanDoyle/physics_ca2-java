
import java.io.*;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Scanner;

public class MainApp
{

    public static void main(String[] args) {
        Scanner kB = new Scanner(System.in);
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("CA2Results.txt"));)
        {
            enterMenu(kB, writer);
        }
        catch (IOException ignored){}
    }

    private static void enterMenu(Scanner kB, BufferedWriter writer) throws IOException {
        boolean done = false;
        while (!done)
        {
            printOptions();
            int option = kB.nextInt();
            switch(option)
            {
                case 1:
                    blockQ1(writer);
                    break;

                case 2:
                    blockQ2(writer);
                    break;

                case 3:
                    blockQ3(kB, writer);
                    break;

                case 4:
                    System.out.println("Saving and Exiting...");
                    done = true;
                    break;
                default:
                    System.out.println("Something went wrong please enter a number between 1 and 4");
            }
        }
        System.out.println("Goodbye");
    }

    private static void printOptions() {
        System.out.println("1: Show Worked Example 1");
        System.out.println("2: Show Worked Example 2");
        System.out.println("3: Enter custom conditions");
        System.out.println("4: Quit");
    }
    private static void blockQ1(BufferedWriter writer) throws IOException {
        double mass = 0.8, uStatic = 0.7, uKinetic = 0.4;
        Vector3 NBar = new Vector3(2,-4,3);
        Block b = new Block(mass,uStatic, uKinetic, NBar);
        writer.write(b.toString());
        for(int i = 0; i < 5; i++)
        {
            System.out.println("Step " + i + ": ");
            rungeKutta(b, .2);
        }
        writer.write(b.updatedToString());

    }

    private static void blockQ2(BufferedWriter writer) throws IOException {
        double mass = 0.5, uStatic = 0.6, uKinetic = 0.5;
        Vector3 NBar = new Vector3(1,-2,12);
        Block b = new Block(mass,uStatic, uKinetic, NBar);
        writer.write(b.toString());
    }

    private static void blockQ3(Scanner kB, BufferedWriter writer) throws IOException {

        writer.write("Custom input: ");
        boolean done = false, customGrav = false;
        double mass = 1, uStatic = 0, uKinetic = 0, userGrav = 0;
        Vector3 NBar = new Vector3();
        while (!done)
        {
            System.out.println("Make the vector for your Nbar");
            NBar = createVector(kB);
            System.out.println("Enter the mass value as a double");
            mass = kB.nextDouble();
            System.out.println("Enter the uStatic value as a double");
            uStatic = kB.nextDouble();
            System.out.println("Enter the uKinetic value as a double");
            uKinetic = kB.nextDouble();
            System.out.println("Enter 2 if you are want to add a custom gravity");
            if (kB.nextInt() == 2)
            {
                customGrav = true;
                System.out.println("Enter your gravity value as a double");
                userGrav = kB.nextDouble();
            }
            System.out.println("Enter 1 if you ared happy with your inputs");
            if (kB.nextInt() == 1)
            {
                done = true;
            }
            else
            {
                System.out.println("Enter the values you want");
            }
        }
        writer.write(kB.toString());
        Block b;
        if (!customGrav)
        {
            b = new Block(mass,uStatic, uKinetic, NBar);
            writer.write(b.toString());
        }
        else
        {
            b = new Block(userGrav,mass,uStatic, uKinetic, NBar);
            writer.write(b.toString());
        }
        for(int i = 0;i < 5; i++)
        {

            rungeKutta(b, .1);
            writer.write("Position at step " + (i + 1) + ": " + b.NBar + "\nVelocity at step " + (i + 1) + ": " + b.velocity);
        }

        writer.write(b.updatedToString());

    }
    private static void ballQ1(BufferedWriter writer) throws IOException {
        double time = 5.0f, radius = 0.05f, ballDensity = 7800f, fluidDensity = 80f, dragCoefficient = 0.1f;
        Vector3 initPos = new Vector3(2,-3,6);
        Vector3 initVelocity = new Vector3(-5,14,2);

        Vector3 rotationForce = new Vector3(-10f/3f,-5f/3f,10f/3f);
        Vector3 WindForce = new Vector3(2f,3f,0f);
        Ball b = new Ball(radius, ballDensity, fluidDensity, dragCoefficient,initPos, initVelocity, rotationForce, WindForce);

        writer.write("\nExample 1: " + b.toString() + "\n");
        for(int i = 0; i < 5;i++)
        {
            System.out.println("Step: " + (i+1)+ " ");
            writer.write("Step " + (i+1)+ ": ");
            rungeKutta(b, .1);
            writer.write("Position: " + b.initPos.toString()+ "\n");
            writer.write("\t    Velocity: " + b.velocity.toString()+ "\n");
        }
    }

    private static void ballQ2(BufferedWriter writer) throws IOException {

        double time = 10.0f, radius = .50f, ballDensity = 90f, fluidDensity = 1.205f, dragCoefficient = 0.01f;
        Vector3 initPos = new Vector3(5f,9f,-3f);
        Vector3 initVelocity = new Vector3(-1f,11f,5f);
        Vector3 rotationForce = new Vector3(-19/7f,6/5f,1f);
        Vector3 WindForce = new Vector3(.5f,-4f,-2f);
        Ball b = new Ball(radius, ballDensity, fluidDensity, dragCoefficient,initPos, initVelocity, rotationForce, WindForce);
        writer.write("Example 2: " + b.toString()+ "\n");
        rungeKutta(b, .1);
        writer.write("Position: " + b.initPos.toString()+ "\n");
        writer.write("Velocity: " + b.velocity.toString()+ "\n");

    }

    private static void ballQ3(Scanner kB, BufferedWriter writer) throws IOException {

        boolean done = false, customGrav = false;
        double time = 0.0, radius = 0.0, ballDensity = 0.0, fluidDensity = 0.0, dragCoefficient = 0.0, userGrav = 9.81;
        System.out.println("Initial Position: ");
        Vector3 initPos = createVector(kB);

        System.out.println("Initial Velocity: ");
        Vector3 initVelocity = createVector(kB);

        System.out.println("Rotation Force: ");
        Vector3 rotationForce = createVector(kB);

        System.out.println("Wind Force: ");
        Vector3 windForce = createVector(kB);

        System.out.println("Time: ");
        time = kB.nextDouble();

        System.out.println("Radius: ");
        radius = kB.nextDouble();

        System.out.println("Ball Density: ");
        ballDensity = kB.nextDouble();

        System.out.println("Fluid Density: ");
        fluidDensity = kB.nextDouble();

        System.out.println("Drag Coefficient: ");
        dragCoefficient = kB.nextDouble();

        System.out.println("Gravity: ");
        userGrav = kB.nextDouble();

        System.out.println("Step Height: ");
        double h = kB.nextDouble();

        Ball b = new Ball(radius, ballDensity, fluidDensity, dragCoefficient,
                initPos, initVelocity, rotationForce, windForce, userGrav);

        writer.write("Example 3: " + b.toString() + "\n");
        rungeKutta(b, h);
        writer.write("Position: " + b.initPos.toString() + "\n");
        writer.write("Velocity: " + b.velocity.toString()+ "\n");
    }

    private static Vector3 createVector(Scanner kB) {

        System.out.println("Enter a value for X");
        double x = kB.nextDouble();
        System.out.println("Enter a value for Y");
        double y = kB.nextDouble();
        System.out.println("Enter a value for Z");
        double z = kB.nextDouble();
        return new Vector3(x, y, z);
    }

    public static void rungeKutta(Ball b, double h)
    {
        Vector3[] k0 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k1 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k2 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k3 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k4 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] pv0 = new Vector3[]{b.initPos, b.velocity};


        k1[0] = pv0[1].scalarMultiply(h);
        k1[1] = b.findAccelerationFromVelocity(b.NetForces).scalarMultiply(h);

        k2[0] = pv0[1].addVectors(k1[1].scalarMultiply(.5));
        k2[1] = b.findAccelerationFromVelocity(k2[0]).scalarMultiply(h);
        k2[0] = k2[0].scalarMultiply(h);

        k3[0] = pv0[1].addVectors(k2[1].scalarMultiply(.5));
        k3[1] = b.findAccelerationFromVelocity(k3[0]).scalarMultiply(h) ;
        k3[0] = k3[0].scalarMultiply(h);

        k4[0] = pv0[1].addVectors(k3[1]);
        k4[1] = b.findAccelerationFromVelocity(k4[0]).scalarMultiply(h);
        k4[0] = k4[0].scalarMultiply(h);

        k0[0] = k1[0];
        k0[0] = k0[0].addVectors(k2[0].scalarMultiply(2));
        k0[0] = k0[0].addVectors(k3[0].scalarMultiply(2));
        k0[0] = k0[0].addVectors(k4[0]);
        k0[0] = k0[0].scalarMultiply((double)1/6);

        k0[1] = k1[1];
        k0[1] = k0[1].addVectors(k2[1].scalarMultiply(2));
        k0[1] = k0[1].addVectors(k3[1].scalarMultiply(2));
        k0[1] = k0[1].addVectors(k4[1]);
        k0[1] = k0[1].scalarMultiply((double)1/6);

        Vector3[] pv1 = new Vector3[]{k0[0].addVectors(pv0[0]),k0[1].addVectors(pv0[1])};

        b.initPos = pv1[0];
        b.velocity = pv1[1];

        System.out.println(pv1[0].toString() + "\n" + pv1[1].toString());
    }

    public static void rungeKutta(Block b, double h)
    {
        if(b.isStatic)
        {
            System.out.println("Block does not move due to static friction");
            return;
        }

        Vector3[] k0 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k1 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k2 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k3 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        Vector3[] k4 = new Vector3[]{new Vector3(0,0,0), new Vector3(0,0,0)};
        if(b.velocity == null)
        {
            b.setVelocity(new Vector3(0,0,0));
        }
        Vector3[] pv0 = new Vector3[]{b.NBar, b.velocity};


        //velocity
        k1[0] = pv0[1].scalarMultiply(h);
        k1[1] = b.findAcceleration(b.Fnet).scalarMultiply(h);

        k2[0] = pv0[1].addVectors(k1[1].scalarMultiply(.5));
        k2[1] = b.findAcceleration(k2[0]).scalarMultiply(h);
        k2[0] = k2[0].scalarMultiply(h);

        k3[0] = pv0[1].addVectors(k2[1].scalarMultiply(.5));
        k3[1] = b.findAcceleration(k3[0]).scalarMultiply(h) ;
        k3[0] = k3[0].scalarMultiply(h);

        k4[0] = pv0[1].addVectors(k3[1]);
        k4[1] = b.findAcceleration(k4[0]).scalarMultiply(h);
        k4[0] = k4[0].scalarMultiply(h);

        k0[0] = k1[0];
        k0[0] = k0[0].addVectors(k2[0].scalarMultiply(2));
        k0[0] = k0[0].addVectors(k3[0].scalarMultiply(2));
        k0[0] = k0[0].addVectors(k4[0]);
        k0[0] = k0[0].scalarMultiply((double)1/6);

        k0[1] = k1[1];
        k0[1] = k0[1].addVectors(k2[1].scalarMultiply(2));
        k0[1] = k0[1].addVectors(k3[1].scalarMultiply(2));
        k0[1] = k0[1].addVectors(k4[1]);
        k0[1] = k0[1].scalarMultiply((double)1/6);

        Vector3[] pv1 = new Vector3[]{k0[0].addVectors(pv0[0]), k0[1].addVectors(pv0[1])};

        b.NBar = pv1[0];
        b.velocity = pv1[1];

        System.out.println(pv1[0].toString() + "\n" + pv1[1].toString());
    }
}