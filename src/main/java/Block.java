public class Block {
    double mass, uStatic, uKinetic, Nlength, FGPlength, Fnlength, FfMax, Fflength;
    Vector3 NBar, NHat, gravity, FGNBar, FGPBar, FGPHat, FNBar, Ffhat, Ffbar, Fnet, Acceleration, velocity;
    boolean isStatic;
    public Block(double mass, double uStatic, double uKinetic, Vector3 NBar)
    {
        this.velocity = new Vector3(0,0,0);
        this.mass = mass;
        this.uStatic = uStatic;
        this.uKinetic = uKinetic;
        this.NBar = NBar;
        this.Nlength = NBar.findLength();
        this.NHat = NBar.normalise();
        this.gravity = findGravity();
        this.FGNBar = findFGN();
        this.FGPBar = findFGP();
        System.out.println("FGP bar =" + FGPBar );
        this.FGPlength = FGPBar.findLength();
        System.out.println("FGP len =" + FGPlength );
        this.FGPHat = FGPBar.normalise();
        this.FNBar = FGNBar.changeSigns();
        this.Fnlength = FNBar.findLength();
        this.FfMax = findFfMax();
        this.isStatic = isStill();//if true object is still and will not slide
        if(isStill())
        {
            System.out.println("Block does not move due to static friction");
        }
        else
        {
            System.out.println("Ball is moving due to kinetic friction");
            this.Ffhat = FGPHat.changeSigns();
            this.Fflength = findFflength();
            this.Ffbar = Ffhat.scalarMultiply(Fflength);
            this.Fnet = FGPBar.addVectors(Ffbar);
            System.out.println("NEt forces = " + Fnet);
            this.Acceleration = getAcceleration();
            System.out.println("Acceleration = " + Acceleration);
            //do Runge kutta
        }
    }

    public Block(double customGravity, double mass, double uStatic, double uKinetic, Vector3 NBar)
    {
        this.velocity = new Vector3(0,0,0);
        this.mass = mass;
        this.uStatic = uStatic;
        this.uKinetic = uKinetic;
        this.NBar = NBar;
        this.Nlength = NBar.findLength();
        this.NHat = NBar.normalise();
        this.gravity = findGravity(customGravity);
        this.FGNBar = findFGN();
        this.FGPBar = findFGP();
        System.out.println("FGP bar =" + FGPBar );
        this.FGPlength = FGPBar.findLength();
        System.out.println("FGP len =" + FGPlength );
        this.FGPHat = FGPBar.normalise();
        this.FNBar = FGNBar.changeSigns();
        this.Fnlength = FNBar.findLength();
        this.FfMax = findFfMax();
        this.isStatic = isStill();//if true object is still and will not slide
        if(isStill())
        {
            System.out.println("Block does not move due to static friction");
        }
        else
        {
            System.out.println("Ball is moving due to kinetic friction");
            this.Ffhat = FGPHat.changeSigns();
            this.Fflength = findFflength();
            this.Ffbar = Ffhat.scalarMultiply(Fflength);
            this.Fnet = FGPBar.addVectors(Ffbar);
            System.out.println("NEt forces = " + Fnet);
            this.Acceleration = getAcceleration();
            System.out.println("Acceleration = " + Acceleration);
            //do Runge kutta
        }
    }

    private Vector3 getAcceleration() { return Fnet.scalarMultiply(1/mass); }


    public Vector3 findAcceleration(Vector3 fNet)
    {
        return fNet.scalarMultiply(1/mass);
    }

    private double findFflength() { return uKinetic * Fnlength; }

    private boolean isStill() { return this.FfMax >= this.FGPlength; }

    private double findFfMax() { return this.uStatic * this.Fnlength; }

    private Vector3 findFGP() {
        return gravity.subtractVectors(FGNBar);
    }

    @Override
    public String toString() {
        return "" +
                "Current Force normal: " + Fnlength +
                "\nCurrent Force friction: " + FfMax +
                "\nCurrent Normal: " + NBar +
                "\nCurrent Gravity: " + gravity +
                "\nCurrent Force Gravity in Normal direction: " + FGNBar +
                "\nCurrent Force of Gravity in Plane direction: " + FGPBar +
                "\nCurrent Force Normal: " + FNBar +
                "\nCurrent net force: " + Fnet;
    }

    public String updatedToString() {
        return "" +
                "Updated Force normal: " + Fnlength +
                "\nUpdated Force friction: " + FfMax +
                "\nUpdated Normal: " + NBar +
                "\nUpdated Gravity: " + gravity +
                "\nUpdated Force Gravity in Normal direction: " + FGNBar +
                "\nUpdated Force of Gravity in Plane direction: " + FGPBar +
                "\nUpdated Force Normal: " + FNBar +
                "\nUpdated net force: " + Fnet;
    }

    private Vector3 findFGN() {
        Vector3 result = new Vector3();
        double FGdotNhat = gravity.dotProduct(NHat);
        return NHat.scalarMultiply(FGdotNhat);
    }

    public Vector3 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector3 velocity) {
        if(velocity == null)
        {
            this.velocity = new Vector3(0,0,0);
        }
        else
        {
            this.velocity = velocity;
        }
    }

    private Vector3 findGravity() {
        return  new Vector3(0,0, (this.mass * -9.81));
    }

    private Vector3 findGravity(double customGravity) {
        return  new Vector3(0,0, (this.mass * -customGravity));
    }
}
