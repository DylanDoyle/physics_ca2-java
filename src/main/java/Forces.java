public class Forces
{
    Vector3 findGravityForce(double gravity, double mass, Vector3 position)
    {
        return new Vector3(0,0,(gravity * mass * position.z));
    }
}
